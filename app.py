import sys
import time
import socket
sys.path.insert(0, '..') # Import the files where the modules are located

from node import CustomNode
from model import Model
from controlplane import ControlPlane

host = socket.gethostbyname(socket.gethostname())

node_1 = CustomNode(host, 33001, 1, verbose=True)
node_2 = CustomNode(host, 33002, 2)
node_3 = CustomNode(host, 33003, 3)
node_4 = CustomNode(host, 33004, 4)
node_5 = CustomNode(host, 33005, 5)
nodes_list = {node_1.id: node_1,
node_2.id: node_2,
node_3.id: node_3,
node_4.id: node_4,
node_5.id: node_5}
controller = ControlPlane(host, 33006, 6, nodes_list)



time.sleep(1)

node_1.start()
node_2.start()
node_3.start()
node_4.start()
node_5.start()
controller.start()

time.sleep(1)

node_1.connect_with_node(host, 33003)
node_2.connect_with_node(host, 33001)
node_2.connect_with_node(host, 33003)
node_3.connect_with_node(host, 33005)
node_4.connect_with_node(host, 33005)
node_5.connect_with_node(host, 33004)

controller.connect_with_node(host, 33003)
controller.connect_with_node(host, 33001)
controller.connect_with_node(host, 33002)
controller.connect_with_node(host, 33005)
controller.connect_with_node(host, 33004)



controller.create_network_topology()

node_1.model.fuel = 0
node_1.model.extra_fuel = 0

time.sleep(30)

node_1.stop()
node_2.stop()
node_3.stop()
node_4.stop()
node_5.stop()
controller.stop()
print('end test')

from p2pnetwork.node import Node
import networkx as nx
#from matplotlib import pyplot as plt
import pandas as pd
import encryption

# Code authored by Lalu Prasad Lenka lenkal@tcd.ie

class ControlPlane(Node):
    def __init__(self, host, port, id, node_dict):
        self.host = host
        self.port = port
        self.id = id
        self.node = ''
        self.nodes = node_dict
        self.stats = []
        # self.policies = []
        self.network = nx.Graph()
        self.status_dict = {0: 'okay' , 1: 'fuel', 2: 'oil', 3: 'battery'}
        self.supply_dict = {1:'extra_fuel', 2: 'extra_oil', 3:'battery' }
        super(ControlPlane, self).__init__(host, port, id)
        self.password = "password"

    def set_available_nodes(self, node_dict):
        #function to find available nodes by pinging port_lists --- host discovery protocol
        #currently hardcoded
        self.nodes = node_dict

    def outbound_node_connected(self, node):
        print("outbound_node_connected (" + self.id + "): " + node.id)
        if node.id not in self.nodes:
            self.nodes.update({node.id: node})

    def create_network_topology(self):
       #function to create network topology ---  link layer discovery protocol
        G = nx.Graph()
        for node in self.nodes.values():
            G.add_node(node)

        for node_id in self.nodes:
            neighbourlist = self.nodes[node_id].all_nodes
            if len(neighbourlist) > 0:
                for neighbour in neighbourlist:
                    if neighbour.id == self.id:
                        continue
                    source = self.nodes[node_id]
                    destination = self.nodes[neighbour.id]
                    if ~G.has_edge(source, destination):
                        G.add_edge(source, destination, weight =1)

        self.network = G

    def print_adjagency_matrix(self):
        print(self.network.adj)

    def get_adjagency_matrix(self):
        return nx.get_adjagency_matrix(self.network.adj)

    def plot_network(self):
        fig,ax = plt.subplots(1,1, figsize = (12,10))
        nx.draw_networkx(self.network,ax = ax)
        plt.xlim(-2,2)
        plt.xlim(-2,2)
        limits = plt.axis("off")
        plt.show()

    def node_message(self, node, data):
        decrypted_data = encryption.decrypt(data, self.password)
        decrypted_data = bytes.decode(decrypted_data)
        decrypted_data = eval(decrypted_data)
        data = decrypted_data
        self.stats.append(data)
        #print("node_message (" + self.id + ") from " + node.id + ": " + str(data))
        if data['status'] != 0 and data['status'] != 5:
            print("Alert!! Node {} needs help with {}".format(node.id, self.status_dict[data['status']]))
            self.find_nearest_help(self.nodes[node.id], data['status'])

    def find_best_route(self, source, destination):
       # returns best route for a given node
        best_route = nx.shortest_path(self.network, source, destination)
        return best_route

    def find_nearest_help(self, source_node, status_code):
        stats_dataframe = pd.DataFrame(self.stats)
        stats_dataframe['source'] = stats_dataframe.source.astype('str')
        stats_dataframe['distance'] = stats_dataframe.apply(lambda x : len(self.find_best_route( source_node , self.nodes[x['source']])) - 1, axis =1)
        stats_dataframe = stats_dataframe.iloc[stats_dataframe.groupby(['source'])['timestamp'].idxmax()].reset_index(drop=True)
        stats_dataframe = stats_dataframe.sort_values(by = [self.supply_dict[status_code],'distance'], ascending=[False, True])

        destination_id = stats_dataframe.iloc[0]['source']
        print("Found node {} for help with {} {}".format(destination_id,self.supply_dict[status_code], stats_dataframe.iloc[0][self.supply_dict[status_code]]))
        path_to_destination = self.find_best_route( source_node , self.nodes[destination_id])

        path_to_destination_id = []
        for node in path_to_destination:
            path_to_destination_id.append(node.id)


        data = {'source_host':source_node.host, 'source_port': source_node.port, 'destination_node_id' : destination_id, 'source_node_id': source_node.id,'path_to_destination': path_to_destination_id, 'status': status_code, 'reverse_path': path_to_destination_id[::-1]}
        for indx, connection in enumerate(self.nodes_outbound):
            if connection.id == source_node.id:
                required_connection = connection
                print(connection.id)
        self.send_to_node(required_connection, encryption.encrypt(str(data),self.password))

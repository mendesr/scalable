echo 'Creating Virtual Environment'
python3 -m venv .venv
echo 'Activating Virtual Environment'
. .venv/bin/activate
echo 'Installing requirements'
pip3 install -r requirements.txt
echo 'Running Simulation'
python3 app.py

echo 'Deactivating Virtual Environment'
deactivate
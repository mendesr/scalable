import threading
import random
import time
from re import search
from encryption import encrypt

# Code authored by Rafael Mendes mendesr@tcd.ie

class Model(threading.Thread):
  max_velocity = 33
  min_velocity = 14
  min_acceleration = 0
  max_acceleration = 2
  critical_oil_levels = 3
  min_oil_levels = 3.5
  max_oil_levels = 5
  max_fuel_levels = 54
  critical_fuel_levels = 0
  min_fuel_levels = 10
  max_battery_charge = 100
  critical_battery_charge = 10
  min_battery_charge = 50
  max_extra_fuel = 10
  min_extra_fuel = 0
  max_extra_oil = 1
  min_extra_oil = 0

  def __init__(self, id, verbose, connection, node):
    super(Model, self).__init__()
    self.verbose = verbose
    self.velocity = round(random.uniform(Model.min_velocity,Model.max_velocity), 1) # m/s
    self.acceleration = 0
    self.oil = round(random.uniform(Model.min_oil_levels, Model.max_oil_levels), 1) # litres
    self.fuel = random.randint(Model.min_fuel_levels, Model.max_fuel_levels) # litres
    self.battery = random.randint(Model.min_battery_charge, Model.max_battery_charge) # percentage
    self.extra_fuel = random.randint(Model.min_extra_fuel, Model.max_extra_fuel) # litres
    self.extra_oil = round(random.uniform(Model.min_extra_oil, Model.max_extra_oil), 1) # litres
    self.longt = round(random.uniform(-6.348, -6.344), 6)
    self.lat = round(random.uniform(53.390,53.392), 6)
    gps = (self.lat, self.longt)
    self.id = id
    self.status = 0
    self.connection = connection
    self.node = node
    timestamp = time.time()
    self.password = "password"

    self.data = {
      "source": id,
      "status": self.status,
      "timestamp": timestamp,
      "velocity": self.velocity,
      "acceleration": self.acceleration,
      "oil": self.oil,
      "fuel": self.fuel,
      "battery": self.battery,
      "extra_fuel": self.extra_fuel,
      "extra_oil": self.extra_oil,
      "gps": gps
    }

    self.terminate_flag = threading.Event()

  def brake(self):
    self.acceleration = -3.4

  def accelerate(self):
    self.acceleration = round(random.uniform(Model.min_acceleration, Model.max_acceleration), 1) # m/s^2

  def stop(self):
    self.brake()
    while self.velocity > 0:
      new_velocity = self.velocity + self.acceleration
      self.velocity = new_velocity if new_velocity > 0 else 0
      time.sleep(1)
    self.acceleration = 0

  def kill(self):
    self.terminate_flag.set()
    print(f"oil: {self.oil}     fuel: {self.fuel}     battery: {self.battery}")
    print("TERMINATING MODEL")

  def check_speed(self):
    if self.velocity<Model.max_velocity and self.acceleration <= 0:
      self.accelerate()
    if self.velocity > Model.max_velocity:
      self.brake()

  def check_oil(self):
    if self.oil < Model.critical_oil_levels:
      if self.extra_oil > 0:
        self.oil += self.extra_oil
        self.extra_oil = 0
      else:
        self.stop()
        self.status = 2

  def check_fuel(self):
    if self.fuel < Model.critical_fuel_levels:
      if self.extra_fuel > 0:
        self.fuel += self.extra_fuel
        self.extra_fuel = 0
      else:
        self.stop()
        self.status = 1

  def check_battery(self):
    if self.battery < Model.critical_battery_charge:
      self.stop()
      self.status = 3

  def update_data(self):
    self.data["status"] = self.status
    self.data["timestamp"] = self.timestamp
    self.data["velocity"] = self.velocity
    self.data["acceleration"] = self.acceleration
    self.data["oil"] = self.oil
    self.data["fuel"] = self.fuel
    self.data["battery"] = self.battery
    self.data["extra_fuel"] = self.extra_fuel
    self.data["extra_oil"] = self.extra_oil
    self.data["gps"] = (self.lat, self.longt)

  def print_status(self):
    status = ""
    if self.status == 0:
      status="CRUISING"
    elif self.status == 4:
      status= "GIVING HELP"
    else: 
      status = "NEED HELP"
    print(f"\n| Node: {self.id} | Status: {status} | Fuel: {self.fuel} | \n | Extra_Fuel: {self.extra_fuel} | Timestamp: {self.timestamp} | \n")

  def run(self):
    while not self.terminate_flag.is_set():
      if self.status == 0:
        self.check_speed()
        self.check_oil()
        self.check_fuel()
        self.check_battery()
        # assuming fuel consumption of 50 Km/L
        self.fuel -= self.velocity / 50000
        # assuming a battery lifespan of ~6 years
        self.battery -= 100 / (6*365*86400)
        # assuming 0.3l used per 1000km
        self.oil -= 0.3 * (self.velocity / 1000000)
        # assuming you're on the M50
        self.longt += self.velocity * -0.0000092395
        self.lat += self.velocity * 0.00000238875
        self.timestamp = time.time()
        self.velocity += self.acceleration
        self.update_data()
      if self.verbose :
        self.print_status()
      self.node.send_to_node(self.connection, encrypt(str(self.data), self.password))
      time.sleep(2)



import base64
import hashlib
from Crypto.Cipher import AES
from Crypto import Random

block = 16
pad = lambda s: s + (block - len(s) % block) * chr(block - len(s) % block)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]

# Code authored by Shriya Vikhram vikhrams@tcd.ie

def encrypt(raw_data, password):
    key = hashlib.sha256(password.encode("utf-8")).digest()
    raw_data = pad(raw_data)
    raw_data = raw_data.encode("utf8")
    iv = Random.new().read(AES.block_size)
    ciphertext = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + ciphertext.encrypt(raw_data))
 
 
def decrypt(enc_data, password):
    private_key = hashlib.sha256(password.encode("utf-8")).digest()
    enc_data = base64.b64decode(enc_data)
    iv = enc_data[:AES.block_size]
    ciphertext = AES.new(private_key, AES.MODE_CBC, iv)
    return unpad(ciphertext.decrypt(enc_data[16:]))

# Group 18 SDN and NVF Implementation

### Implementation
Our system is comprised of a network of Nodes which can send messages to each other and a controller which receives data from all nodes in the network. Each node is running a model to simulate vehicle sensors these being: speed, acceleration, oil, fuel, battery, extra oil, extra fuel. The model also implements acceleration and braking as two actuators.

### Simulation
To run our simulation please run the `run.sh` script which will create and acitvate a Python virtual environment, install the required depedencies and execute `app.py`. The simulation will ouput several status messages to the console containing vehicle statuses as well as logs to show a message's path through our network of nodes. This assumes that python virtual environments are installed in the machine running the simulation. If not please install them using `sudo apt install python3.8-venv`. If something goes wrong and you cannot execute the script, you can simply execute the `app.py` script to start our simulation (provided all depedencies are installed)

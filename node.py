import time
import socket
import threading
from p2pnetwork.node import Node
from model import Model
import encryption

# Code co-authored by Rafael Mendes mendesr@tcd.ie, Lalu Prasad Lenka lenkal@tcd.ie, Shriya Vikhram vikhrams@tcd.ie

class CustomNode (Node):

    # Python class constructor
    def __init__(self, host, port, id=None, callback=None, max_connections=0, verbose=False):
        super(CustomNode, self).__init__(host, port, id, callback, max_connections)
        print("CustomNode: Started")
        self.verbose = verbose
        self.password = "password"

    def generate_data(self):
        for conn_node in self.nodes_inbound:
            if (conn_node.id == '6'):
                self.model = Model(self.id, self.verbose, conn_node, self)
                self.model.start()
        
    def node_message(self, node, data):
        decrypted_data = encryption.decrypt(data, self.password)
        decrypted_data = bytes.decode(decrypted_data)
        decrypted_data = eval(decrypted_data)
        data = decrypted_data
        print("node_message (" + self.id + ") from " + node.id + ": " + str(data))
        if "path_to_destination" in data:
            if  len(data["path_to_destination"]) > 0:
                if data["source_node_id"] == self.id:
                    data["path_to_destination"].pop(0)
                    self.model.status = 0
                for next_node in self.all_nodes:
                    if len(data["path_to_destination"]) > 0 and data["path_to_destination"][0] == next_node.id:
                        data["path_to_destination"].pop(0)
                        self.send_to_node(next_node, encryption.encrypt(str(data), self.password))
            
            elif (len(data["path_to_destination"]) == 0 and data["destination_node_id"] == self.id):
                if data["status"] == 1:
                    source_host = data["source_host"]
                    source_port = data["source_port"]
                    print(f"\nGiving Help to: {source_host}:{source_port}  from: {self.host}:{self.port}\n")
                    data["supply"] = self.model.extra_fuel
                    self.model.print_status()
                    self.model.extra_fuel = 0
                    self.model.print_status()
                elif data["status"] == 2:
                    data["supply"] = self.model.extra_oil
                    self.model.extra_fuel = 0
                elif data["status"] == 3:
                    data["supply"] = 20
                    self.model.extra_fuel -= 20

                data["reverse_path"].pop(0)
                for next_node in self.all_nodes:
                    if len(data["reverse_path"]) > 0 and data["reverse_path"][0] == next_node.id:
                        data["reverse_path"].pop(0)
                        self.send_to_node(next_node, encryption.encrypt(str(data), self.password))
                        
            elif (len(data["path_to_destination"]) == 0 and data["source_node_id"] != self.id):
                for next_node in self.all_nodes:
                    if len(data["reverse_path"]) > 0 and data["reverse_path"][0] == next_node.id:
                        data["reverse_path"].pop(0)
                        self.send_to_node(next_node, encryption.encrypt(str(data), self.password))
            else:
                if data["status"] == 1:
                    self.model.fuel += data["supply"]
                    self.model.status = 0
                elif data["status"] == 2:
                    self.model.fuel += data["oil"]
                    self.model.status = 0
                elif data["status"] == 3:
                    self.model.fuel += data["battery"]
                    self.model.status = 0

    def inbound_node_connected(self, node):
        if node.id == '6':
            self.generate_data()
            print("inbound_node_connected: (" + self.id + "): " + node.id)

    def node_request_to_stop(self):
        self.model.kill()
        self.model.join()